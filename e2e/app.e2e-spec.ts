import { ExTestPage } from './app.po';

describe('ex-test App', function() {
  let page: ExTestPage;

  beforeEach(() => {
    page = new ExTestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
