import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2';
import 'rxjs/Rx';

@Injectable()
export class ProductsService {

productsObservable;

getProducts(){
  return this.productsObservable = this.af.database.list('/products');
}

getProductsWithCategory(){
    this.productsObservable = this.af.database.list('/products').map(
      products =>{
        products.map(
          product =>{
            product.catName =[];
            product.catName.push(this.af.database.object('/category/'+product.category))
          }
        );
        return products;
      }
    )
    return this.productsObservable;
  }

deleteProduct(product){
  this.af.database.object('/products/'+product.$key).remove();
}

  constructor(private af:AngularFire) { }

}
