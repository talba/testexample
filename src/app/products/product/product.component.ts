import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Product } from './product';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  inputs: ['product']
})
export class ProductComponent implements OnInit {

  product:Product;

  @Output() deleteEvent = new EventEmitter<Product>();

  startDelete(){
    this.deleteEvent.emit(this.product);
  }

  constructor() { }

  ngOnInit() {
  }

}
