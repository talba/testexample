import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-products-with-category',
  templateUrl: './products-with-category.component.html',
  styleUrls: ['./products-with-category.component.css']
})
export class ProductsWithCategoryComponent implements OnInit {

  products;

  constructor(private _productsService:ProductsService) { }

  ngOnInit() {
    this._productsService.getProductsWithCategory().subscribe(productsData => {this.products=productsData})
  }

}
