import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {AngularFireModule} from 'angularfire2';
import { ProductsService } from './products/products.service';

import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './products/product/product.component';
import { ProductsWithCategoryComponent } from './products/products-with-category/products-with-category.component';

export const firebaseConfig = {
    apiKey: "AIzaSyAmXkb1CnKJIyGXNcmrF_dLCa_PwuNkV-A",
    authDomain: "extest-fc8f3.firebaseapp.com",
    databaseURL: "https://extest-fc8f3.firebaseio.com",
    storageBucket: "extest-fc8f3.appspot.com",
    messagingSenderId: "356964853328"
  }

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    ProductComponent,
    ProductsWithCategoryComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [ProductsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
